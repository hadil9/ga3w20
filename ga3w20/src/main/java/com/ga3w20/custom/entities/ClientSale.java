package com.ga3w20.custom.entities;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * Represent total sales of books purchased by client
 * 
 * @author Hadil Elhashani
 */
public class ClientSale {
    
    private String firstname;
    private String lastName;
    private String bookTitle;
    private BigDecimal bookPrice;
    private Date datePurchased;

    public ClientSale(String firstname, String lastName, String bookTitle, BigDecimal bookPrice, Date datePurchased) {
        this.firstname = firstname;
        this.lastName = lastName;
        this.bookTitle = bookTitle;
        this.bookPrice = bookPrice;
        this.datePurchased = datePurchased;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public BigDecimal getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(BigDecimal bookPrice) {
        this.bookPrice = bookPrice;
    }

    public Date getDatePurchased() {
        return datePurchased;
    }

    public void setDatePurchased(Date datePurchased) {
        this.datePurchased = datePurchased;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.firstname);
        hash = 67 * hash + Objects.hashCode(this.lastName);
        hash = 67 * hash + Objects.hashCode(this.bookTitle);
        hash = 67 * hash + Objects.hashCode(this.bookPrice);
        hash = 67 * hash + Objects.hashCode(this.datePurchased);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClientSale other = (ClientSale) obj;
        if (!Objects.equals(this.firstname, other.firstname)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.bookTitle, other.bookTitle)) {
            return false;
        }
        if (!Objects.equals(this.bookPrice, other.bookPrice)) {
            return false;
        }
        if (!Objects.equals(this.datePurchased, other.datePurchased)) {
            return false;
        }
        return true;
    }
    
}
