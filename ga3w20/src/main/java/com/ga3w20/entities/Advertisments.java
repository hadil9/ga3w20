package com.ga3w20.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author 1738714 Yongchao
 */
@Entity
@Table(name = "advertisments", catalog = "ga3w20", schema = "")

public class Advertisments implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "image_url")
    private String imageUrl;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_active")
    private boolean isActive;
    @Size(max = 1000)
    @Column(name = "link")
    private String link;

    public Advertisments() {
    }

    public Advertisments(Integer id) {
        this.id = id;
    }

    public Advertisments(Integer id, String link, String imageUrl, boolean isActive) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.link = link;
        this.isActive = isActive;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Advertisments)) {
            return false;
        }
        Advertisments other = (Advertisments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)) || (this.imageUrl != null && !this.imageUrl.equals(other.imageUrl))
        || (this.link != null && !this.link.equals(other.link)) || !(this.isActive == other.isActive)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Advertisments Id=" + this.id + 
                ", imageUrl="+ this.imageUrl +
                ", link=" + this.link +
                ", isActive="+ this.isActive;
    }
    
}
