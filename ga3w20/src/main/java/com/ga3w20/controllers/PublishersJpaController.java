package com.ga3w20.controllers;

import com.ga3w20.exceptions.IllegalOrphanException;
import com.ga3w20.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.ga3w20.entities.Books;
import com.ga3w20.entities.Publishers;
import com.ga3w20.exceptions.RollbackFailureException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Hadil Elhashani
 */
@Named
@SessionScoped
public class PublishersJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(PublishersJpaController.class);
    
    private Publishers publisher;

    public Publishers getPublisher() {
        if (publisher == null) {
            publisher = new Publishers();
        }
        return publisher;
    }

    public void setPublisher(Publishers author) {
        this.publisher = author;
    }
    
    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "ga3w20PU")
    private EntityManager em;
    
    public PublishersJpaController() {
    }

    public void create(Publishers publishers) throws RollbackFailureException {
        if (publishers.getBooksList() == null) {
            publishers.setBooksList(new ArrayList<Books>());
        }
        try {
           utx.begin();
            List<Books> attachedBooksList = new ArrayList<Books>();
            for (Books booksListBooksToAttach : publishers.getBooksList()) {
                booksListBooksToAttach = em.getReference(booksListBooksToAttach.getClass(), booksListBooksToAttach.getIsbn());
                attachedBooksList.add(booksListBooksToAttach);
            }
            publishers.setBooksList(attachedBooksList);
            em.persist(publishers);
            for (Books booksListBooks : publishers.getBooksList()) {
                Publishers oldPublisherIdOfBooksListBooks = booksListBooks.getPublisherId();
                booksListBooks.setPublisherId(publishers);
                booksListBooks = em.merge(booksListBooks);
                if (oldPublisherIdOfBooksListBooks != null) {
                    oldPublisherIdOfBooksListBooks.getBooksList().remove(booksListBooks);
                    oldPublisherIdOfBooksListBooks = em.merge(oldPublisherIdOfBooksListBooks);
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

    public void edit(Publishers publishers) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            Publishers persistentPublishers = em.find(Publishers.class, publishers.getId());
            List<Books> booksListOld = persistentPublishers.getBooksList();
            List<Books> booksListNew = publishers.getBooksList();
            List<String> illegalOrphanMessages = null;
            for (Books booksListOldBooks : booksListOld) {
                if (!booksListNew.contains(booksListOldBooks)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Books " + booksListOldBooks + " since its publisherId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Books> attachedBooksListNew = new ArrayList<Books>();
            for (Books booksListNewBooksToAttach : booksListNew) {
                booksListNewBooksToAttach = em.getReference(booksListNewBooksToAttach.getClass(), booksListNewBooksToAttach.getIsbn());
                attachedBooksListNew.add(booksListNewBooksToAttach);
            }
            booksListNew = attachedBooksListNew;
            publishers.setBooksList(booksListNew);
            publishers = em.merge(publishers);
            for (Books booksListNewBooks : booksListNew) {
                if (!booksListOld.contains(booksListNewBooks)) {
                    Publishers oldPublisherIdOfBooksListNewBooks = booksListNewBooks.getPublisherId();
                    booksListNewBooks.setPublisherId(publishers);
                    booksListNewBooks = em.merge(booksListNewBooks);
                    if (oldPublisherIdOfBooksListNewBooks != null && !oldPublisherIdOfBooksListNewBooks.equals(publishers)) {
                        oldPublisherIdOfBooksListNewBooks.getBooksList().remove(booksListNewBooks);
                        oldPublisherIdOfBooksListNewBooks = em.merge(oldPublisherIdOfBooksListNewBooks);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = publishers.getId();
                if (findPublishers(id) == null) {
                    throw new NonexistentEntityException("The publishers with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } 
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Publishers publishers;
            try {
                publishers = em.getReference(Publishers.class, id);
                publishers.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The publishers with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Books> booksListOrphanCheck = publishers.getBooksList();
            for (Books booksListOrphanCheckBooks : booksListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Publishers (" + publishers + ") cannot be destroyed since the Books " + booksListOrphanCheckBooks + " in its booksList field has a non-nullable publisherId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(publishers);
            utx.commit();
        }catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Publishers> findPublishersEntities() {
        LOG.info("getting all the publishers");
        return findPublishersEntities(true, -1, -1);
    }

    public List<Publishers> findPublishersEntities(int maxResults, int firstResult) {
        return findPublishersEntities(false, maxResults, firstResult);
    }

    private List<Publishers> findPublishersEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Publishers.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Publishers findPublishers(Integer id) {
        return em.find(Publishers.class, id);
    }

    public int getPublishersCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Publishers> rt = cq.from(Publishers.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }  
}
