package com.ga3w20.controllers;

import com.ga3w20.exceptions.NonexistentEntityException;
import com.ga3w20.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.ga3w20.entities.Books;
import com.ga3w20.entities.Sales;
import com.ga3w20.entities.SoldBooks;
import com.ga3w20.entities.Users;
import com.ga3w20.exceptions.IllegalOrphanException;
import com.ga3w20.exceptions.RollbackFailureException;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.ParameterExpression;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Franco G. Moro 1738714
 * @author Hadil Elhashani
 */
@Named
@SessionScoped
public class SoldBooksJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(SoldBooksJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "ga3w20PU")
    private EntityManager em;

    public SoldBooksJpaController() {
    }

    public void create(SoldBooks soldBooks) throws PreexistingEntityException, Exception {
        try {
            utx.begin();
            Books booksIsbn = soldBooks.getBooksIsbn();
            if (booksIsbn != null) {
                booksIsbn = em.getReference(booksIsbn.getClass(), booksIsbn.getIsbn());
                soldBooks.setBooksIsbn(booksIsbn);
            }
            Sales salesId = soldBooks.getSalesId();
            if (salesId != null) {
                salesId = em.getReference(salesId.getClass(), salesId.getId());
                soldBooks.setSalesId(salesId);
            }
            em.persist(soldBooks);
            if (booksIsbn != null) {
                booksIsbn.getSoldBooksList().add(soldBooks);
                booksIsbn = em.merge(booksIsbn);
            }
            if (salesId != null) {
                salesId.getSoldBooksList().add(soldBooks);
                salesId = em.merge(salesId);
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

    public void edit(SoldBooks soldBooks) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            SoldBooks persistentSoldBooks = em.find(SoldBooks.class, soldBooks.getId());
            Books booksIsbnOld = persistentSoldBooks.getBooksIsbn();
            Books booksIsbnNew = soldBooks.getBooksIsbn();
            Sales salesIdOld = persistentSoldBooks.getSalesId();
            Sales salesIdNew = soldBooks.getSalesId();
            if (booksIsbnNew != null) {
                booksIsbnNew = em.getReference(booksIsbnNew.getClass(), booksIsbnNew.getIsbn());
                soldBooks.setBooksIsbn(booksIsbnNew);
            }
            if (salesIdNew != null) {
                salesIdNew = em.getReference(salesIdNew.getClass(), salesIdNew.getId());
                soldBooks.setSalesId(salesIdNew);
            }
            soldBooks = em.merge(soldBooks);
            if (booksIsbnOld != null && !booksIsbnOld.equals(booksIsbnNew)) {
                booksIsbnOld.getSoldBooksList().remove(soldBooks);
                booksIsbnOld = em.merge(booksIsbnOld);
            }
            if (booksIsbnNew != null && !booksIsbnNew.equals(booksIsbnOld)) {
                booksIsbnNew.getSoldBooksList().add(soldBooks);
                booksIsbnNew = em.merge(booksIsbnNew);
            }
            if (salesIdOld != null && !salesIdOld.equals(salesIdNew)) {
                salesIdOld.getSoldBooksList().remove(soldBooks);
                salesIdOld = em.merge(salesIdOld);
            }
            if (salesIdNew != null && !salesIdNew.equals(salesIdOld)) {
                salesIdNew.getSoldBooksList().add(soldBooks);
                salesIdNew = em.merge(salesIdNew);
            }
            utx.commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = soldBooks.getId();
                if (findSoldBooks(id) == null) {
                    throw new NonexistentEntityException("The soldBooks with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            SoldBooks soldBooks;
            try {
                soldBooks = em.getReference(SoldBooks.class, id);
                soldBooks.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The soldBooks with id " + id + " no longer exists.", enfe);
            }
            Books booksIsbn = soldBooks.getBooksIsbn();
            if (booksIsbn != null) {
                booksIsbn.getSoldBooksList().remove(soldBooks);
                booksIsbn = em.merge(booksIsbn);
            }
            Sales salesId = soldBooks.getSalesId();
            if (salesId != null) {
                salesId.getSoldBooksList().remove(soldBooks);
                salesId = em.merge(salesId);
            }
            em.remove(soldBooks);
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<SoldBooks> findSoldBooksEntities() {
        return findSoldBooksEntities(true, -1, -1);
    }

    public List<SoldBooks> findSoldBooksEntities(int maxResults, int firstResult) {
        return findSoldBooksEntities(false, maxResults, firstResult);
    }

    private List<SoldBooks> findSoldBooksEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(SoldBooks.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public SoldBooks findSoldBooks(Integer id) {
        return em.find(SoldBooks.class, id);
    }

    public int getSoldBooksCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<SoldBooks> rt = cq.from(SoldBooks.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    /**
     * Retrieve all the books (usually one) that a user bought, if any, as to show to the
     * user to buy the book or to download it.
     * 
     * @param userId
     * @param bookIsbn
     * @return list of SoldBooks, the book that a user bought
     * @author Alexander Berestetskyy
     */
    public List<SoldBooks> findSaleOfBookWithUser(Integer userId, String bookIsbn){
        CriteriaBuilder cb3 = em.getCriteriaBuilder();
        CriteriaQuery<SoldBooks> q3 = cb3.createQuery(SoldBooks.class);
        Root<SoldBooks> soldbooks = q3.from(SoldBooks.class);
        Join<Sales, SoldBooks> sales = soldbooks.join("salesId");
        Join<Books, SoldBooks> books = soldbooks.join("booksIsbn");
        Join<Users, Sales> users = sales.join("usersId");
        ParameterExpression<String> isbn = cb3.parameter(String.class); 
        ParameterExpression<Integer> uId = cb3.parameter(Integer.class);
        q3.select(soldbooks);
        q3.where(cb3.and(cb3.equal(books.get("isbn"), isbn), cb3.equal(users.get("id"), uId)));
        TypedQuery<SoldBooks> query = em.createQuery(q3);
        query.setParameter(isbn, bookIsbn);
        query.setParameter(uId, userId);
        List<SoldBooks> soldbooksList = query.getResultList();
        return soldbooksList;
    }
}
