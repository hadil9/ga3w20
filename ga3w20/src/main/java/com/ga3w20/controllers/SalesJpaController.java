package com.ga3w20.controllers;

import com.ga3w20.entities.Books;
import com.ga3w20.exceptions.IllegalOrphanException;
import com.ga3w20.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.ga3w20.entities.Users;
import com.ga3w20.entities.SoldBooks;
import java.util.ArrayList;
import java.util.List;
import com.ga3w20.entities.Sales;
import com.ga3w20.exceptions.RollbackFailureException;
import java.util.logging.Level;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.ParameterExpression;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Franco G. Moro 1738714
 * @author Hadil Elhashani
 */
@Named
@SessionScoped
public class SalesJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(SalesJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "ga3w20PU")
    private EntityManager em;

    public SalesJpaController() {
    }

    public void create(Sales sales) throws RollbackFailureException {
        if (sales.getSoldBooksList() == null) {
            sales.setSoldBooksList(new ArrayList<SoldBooks>());
        }
        try {
            utx.begin();
            Users usersId = sales.getUsersId();
            if (usersId != null) {
                usersId = em.getReference(usersId.getClass(), usersId.getId());
                sales.setUsersId(usersId);
            }
            List<SoldBooks> attachedSoldBooksList = new ArrayList<SoldBooks>();
            for (SoldBooks soldBooksListSoldBooksToAttach : sales.getSoldBooksList()) {
                soldBooksListSoldBooksToAttach = em.getReference(soldBooksListSoldBooksToAttach.getClass(), soldBooksListSoldBooksToAttach.getId());
                attachedSoldBooksList.add(soldBooksListSoldBooksToAttach);
            }
            sales.setSoldBooksList(attachedSoldBooksList);
            em.persist(sales);
            if (usersId != null) {
                usersId.getSalesList().add(sales);
                usersId = em.merge(usersId);
            }
            for (SoldBooks soldBooksListSoldBooks : sales.getSoldBooksList()) {
                Sales oldSalesIdOfSoldBooksListSoldBooks = soldBooksListSoldBooks.getSalesId();
                soldBooksListSoldBooks.setSalesId(sales);
                soldBooksListSoldBooks = em.merge(soldBooksListSoldBooks);
                if (oldSalesIdOfSoldBooksListSoldBooks != null) {
                    oldSalesIdOfSoldBooksListSoldBooks.getSoldBooksList().remove(soldBooksListSoldBooks);
                    oldSalesIdOfSoldBooksListSoldBooks = em.merge(oldSalesIdOfSoldBooksListSoldBooks);
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

    public void edit(Sales sales) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            Sales persistentSales = em.find(Sales.class, sales.getId());
            Users usersIdOld = persistentSales.getUsersId();
            Users usersIdNew = sales.getUsersId();
            List<SoldBooks> soldBooksListOld = persistentSales.getSoldBooksList();
            List<SoldBooks> soldBooksListNew = sales.getSoldBooksList();
//            List<Invoices> invoicesListOld = persistentSales.getInvoicesList();
//            List<Invoices> invoicesListNew = sales.getInvoicesList();
            List<String> illegalOrphanMessages = null;
            for (SoldBooks soldBooksListOldSoldBooks : soldBooksListOld) {
                if (!soldBooksListNew.contains(soldBooksListOldSoldBooks)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SoldBooks " + soldBooksListOldSoldBooks + " since its salesId field is not nullable.");
                }
            }
//            for (Invoices invoicesListOldInvoices : invoicesListOld) {
//                if (!invoicesListNew.contains(invoicesListOldInvoices)) {
//                    if (illegalOrphanMessages == null) {
//                        illegalOrphanMessages = new ArrayList<String>();
//                    }
//                    illegalOrphanMessages.add("You must retain Invoices " + invoicesListOldInvoices + " since its salesId field is not nullable.");
//                }
//            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (usersIdNew != null) {
                usersIdNew = em.getReference(usersIdNew.getClass(), usersIdNew.getId());
                sales.setUsersId(usersIdNew);
            }
            List<SoldBooks> attachedSoldBooksListNew = new ArrayList<SoldBooks>();
            for (SoldBooks soldBooksListNewSoldBooksToAttach : soldBooksListNew) {
                soldBooksListNewSoldBooksToAttach = em.getReference(soldBooksListNewSoldBooksToAttach.getClass(), soldBooksListNewSoldBooksToAttach.getId());
                attachedSoldBooksListNew.add(soldBooksListNewSoldBooksToAttach);
            }
            soldBooksListNew = attachedSoldBooksListNew;
            sales.setSoldBooksList(soldBooksListNew);
//            List<Invoices> attachedInvoicesListNew = new ArrayList<Invoices>();
//            for (Invoices invoicesListNewInvoicesToAttach : invoicesListNew) {
//                invoicesListNewInvoicesToAttach = em.getReference(invoicesListNewInvoicesToAttach.getClass(), invoicesListNewInvoicesToAttach.getId());
//                attachedInvoicesListNew.add(invoicesListNewInvoicesToAttach);
//            }
//            invoicesListNew = attachedInvoicesListNew;
//            sales.setInvoicesList(invoicesListNew);
            sales = em.merge(sales);
            if (usersIdOld != null && !usersIdOld.equals(usersIdNew)) {
                usersIdOld.getSalesList().remove(sales);
                usersIdOld = em.merge(usersIdOld);
            }
            if (usersIdNew != null && !usersIdNew.equals(usersIdOld)) {
                usersIdNew.getSalesList().add(sales);
                usersIdNew = em.merge(usersIdNew);
            }
            for (SoldBooks soldBooksListNewSoldBooks : soldBooksListNew) {
                if (!soldBooksListOld.contains(soldBooksListNewSoldBooks)) {
                    Sales oldSalesIdOfSoldBooksListNewSoldBooks = soldBooksListNewSoldBooks.getSalesId();
                    soldBooksListNewSoldBooks.setSalesId(sales);
                    soldBooksListNewSoldBooks = em.merge(soldBooksListNewSoldBooks);
                    if (oldSalesIdOfSoldBooksListNewSoldBooks != null && !oldSalesIdOfSoldBooksListNewSoldBooks.equals(sales)) {
                        oldSalesIdOfSoldBooksListNewSoldBooks.getSoldBooksList().remove(soldBooksListNewSoldBooks);
                        oldSalesIdOfSoldBooksListNewSoldBooks = em.merge(oldSalesIdOfSoldBooksListNewSoldBooks);
                    }
                }
            }
//            for (Invoices invoicesListNewInvoices : invoicesListNew) {
//                if (!invoicesListOld.contains(invoicesListNewInvoices)) {
//                    Sales oldSalesIdOfInvoicesListNewInvoices = invoicesListNewInvoices.getSalesId();
//                    invoicesListNewInvoices.setSalesId(sales);
//                    invoicesListNewInvoices = em.merge(invoicesListNewInvoices);
//                    if (oldSalesIdOfInvoicesListNewInvoices != null && !oldSalesIdOfInvoicesListNewInvoices.equals(sales)) {
//                        oldSalesIdOfInvoicesListNewInvoices.getInvoicesList().remove(invoicesListNewInvoices);
//                        oldSalesIdOfInvoicesListNewInvoices = em.merge(oldSalesIdOfInvoicesListNewInvoices);
//                    }
//                }
//            }
            utx.commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sales.getId();
                if (findSales(id) == null) {
                    throw new NonexistentEntityException("The sales with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Sales sales;
            try {
                sales = em.getReference(Sales.class, id);
                sales.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sales with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<SoldBooks> soldBooksListOrphanCheck = sales.getSoldBooksList();
            for (SoldBooks soldBooksListOrphanCheckSoldBooks : soldBooksListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sales (" + sales + ") cannot be destroyed since the SoldBooks " + soldBooksListOrphanCheckSoldBooks + " in its soldBooksList field has a non-nullable salesId field.");
            }
//            List<Invoices> invoicesListOrphanCheck = sales.getInvoicesList();
//            for (Invoices invoicesListOrphanCheckInvoices : invoicesListOrphanCheck) {
//                if (illegalOrphanMessages == null) {
//                    illegalOrphanMessages = new ArrayList<String>();
//                }
//                illegalOrphanMessages.add("This Sales (" + sales + ") cannot be destroyed since the Invoices " + invoicesListOrphanCheckInvoices + " in its invoicesList field has a non-nullable salesId field.");
//            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Users usersId = sales.getUsersId();
            if (usersId != null) {
                usersId.getSalesList().remove(sales);
                usersId = em.merge(usersId);
            }
            em.remove(sales);
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Sales> findSalesEntities() {
        return findSalesEntities(true, -1, -1);
    }

    public List<Sales> findSalesEntities(int maxResults, int firstResult) {
        return findSalesEntities(false, maxResults, firstResult);
    }

    private List<Sales> findSalesEntities(boolean all, int maxResults, int firstResult) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Sales> sales = cq.from(Sales.class);
        cq.select(sales).where(cb.equal(sales.get("isRemoved"), Boolean.FALSE));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Sales findSales(Integer id) {
        return em.find(Sales.class, id);
    }

    public int getSalesCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Sales> rt = cq.from(Sales.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    /**
     *
     *
     * @Hadil Elhashani
     * @param salesId
     * @return
     */
    public Sales updateSalesRemovalStatus(Integer salesId) {
        LOG.debug("updateSalesRemovalStatus");
        try {
            utx.begin();
        } catch (NotSupportedException | SystemException ex) {
            java.util.logging.Logger.getLogger(BooksJpaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOG.debug("salesId = " + salesId);
        Sales persistentSale = em.find(Sales.class, salesId);
        persistentSale.setIsRemoved(true);

        try {
            utx.commit();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "Sale removed salesId = " + salesId));
        } catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException | SystemException ex) {
            java.util.logging.Logger.getLogger(BooksJpaController.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Sale not removed salesId = " + salesId));
        }
        return persistentSale;
    }

    /**
     * Takes a string as input and return 
     * sales list that matches string with
     * user first name, last name, and book title
     * 
     * @author Hadil Elhashani
     * @param searchTerm
     * @return
     */
    public List<Sales> findSalesByUserOrBookTitle(String searchTerm) {
        LOG.debug("findSalesByUserOrBookTitle");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Sales> q = cb.createQuery(Sales.class);
        Root<Sales> sales = q.from(Sales.class);

        LOG.debug("searchTerm = " + searchTerm);

        Join<Sales, Users> users = sales.join("usersId");
        Join<Sales, SoldBooks> soldbooks = sales.join("soldBooksList");
        Join<Books, SoldBooks> books = soldbooks.join("booksIsbn");

        ParameterExpression<String> title = cb.parameter(String.class);
        ParameterExpression<String> fname = cb.parameter(String.class);
        ParameterExpression<String> lname = cb.parameter(String.class);

        q.select(sales).where(cb.or(cb.like(users.get("firstName"), fname),
                cb.like(users.get("lastName"), lname),
                cb.like(books.get("title"), title)))
                .groupBy(sales.get("usersId"));
        TypedQuery<Sales> query = em.createQuery(q);
        query.setParameter(fname, "%" + searchTerm + "%");
        query.setParameter(lname, "%" + searchTerm + "%");
        query.setParameter(title, "%" + searchTerm + "%");
        List<Sales> salesList = query.getResultList();
        return salesList;
    }
}
