package com.ga3w20.controllers;

import com.ga3w20.custom.entities.AuthorSale;
import com.ga3w20.custom.entities.ClientSale;
import com.ga3w20.custom.entities.Inventory;
import com.ga3w20.custom.entities.PublisherSale;
import com.ga3w20.custom.entities.TopClient;
import com.ga3w20.custom.entities.TopSeller;
import com.ga3w20.exceptions.IllegalOrphanException;
import com.ga3w20.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.ga3w20.entities.Authors;
import com.ga3w20.entities.Books;
import com.ga3w20.entities.Publishers;
import com.ga3w20.entities.Format;
import com.ga3w20.entities.Genre;
import java.util.ArrayList;
import java.util.List;
import com.ga3w20.entities.SoldBooks;
import com.ga3w20.entities.Reviews;
import com.ga3w20.entities.Sales;
import com.ga3w20.entities.Users;
import com.ga3w20.exceptions.RollbackFailureException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Date;
import java.util.logging.Level;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Subquery;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1738714
 * @author Hadil Elhashani
 * @author Alexander Berestetskyy
 */
@Named
@SessionScoped
public class BooksJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(BooksJpaController.class);

    private Books book;

    public Books getBook() {
        if (book == null) {
            book = new Books();
        }
        return book;
    }

    public void setBook(Books book) {
        this.book = book;
    }

    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "ga3w20PU")
    private EntityManager em;

    public BooksJpaController() {
    }

    public void create(Books books) throws RollbackFailureException {
        if (books.getFormatList() == null) {
            books.setFormatList(new ArrayList<Format>());
        }
        if (books.getSoldBooksList() == null) {
            books.setSoldBooksList(new ArrayList<SoldBooks>());
        }
        if (books.getReviewsList() == null) {
            books.setReviewsList(new ArrayList<Reviews>());
        }
        try {
            utx.begin();
            Authors authorId = books.getAuthorId();
            if (authorId != null) {
                authorId = em.getReference(authorId.getClass(), authorId.getId());
                books.setAuthorId(authorId);
            }
            Publishers publisherId = books.getPublisherId();
            if (publisherId != null) {
                publisherId = em.getReference(publisherId.getClass(), publisherId.getId());
                books.setPublisherId(publisherId);
            }
            List<Format> attachedFormatList = new ArrayList<Format>();
            for (Format formatListFormatToAttach : books.getFormatList()) {
                formatListFormatToAttach = em.getReference(formatListFormatToAttach.getClass(), formatListFormatToAttach.getId());
                attachedFormatList.add(formatListFormatToAttach);
            }
            books.setFormatList(attachedFormatList);
            List<SoldBooks> attachedSoldBooksList = new ArrayList<SoldBooks>();
            for (SoldBooks soldBooksListSoldBooksToAttach : books.getSoldBooksList()) {
                soldBooksListSoldBooksToAttach = em.getReference(soldBooksListSoldBooksToAttach.getClass(), soldBooksListSoldBooksToAttach.getId());
                attachedSoldBooksList.add(soldBooksListSoldBooksToAttach);
            }
            books.setSoldBooksList(attachedSoldBooksList);
            List<Reviews> attachedReviewsList = new ArrayList<Reviews>();
            for (Reviews reviewsListReviewsToAttach : books.getReviewsList()) {
                reviewsListReviewsToAttach = em.getReference(reviewsListReviewsToAttach.getClass(), reviewsListReviewsToAttach.getId());
                attachedReviewsList.add(reviewsListReviewsToAttach);
            }
            books.setReviewsList(attachedReviewsList);
            em.persist(books);
            if (authorId != null) {
                authorId.getBooksList().add(books);
                authorId = em.merge(authorId);
            }
            if (publisherId != null) {
                publisherId.getBooksList().add(books);
                publisherId = em.merge(publisherId);
            }
            for (Format formatListFormat : books.getFormatList()) {
                formatListFormat.getBooksList().add(books);
                formatListFormat = em.merge(formatListFormat);
            }
            for (SoldBooks soldBooksListSoldBooks : books.getSoldBooksList()) {
                Books oldBooksIsbnOfSoldBooksListSoldBooks = soldBooksListSoldBooks.getBooksIsbn();
                soldBooksListSoldBooks.setBooksIsbn(books);
                soldBooksListSoldBooks = em.merge(soldBooksListSoldBooks);
                if (oldBooksIsbnOfSoldBooksListSoldBooks != null) {
                    oldBooksIsbnOfSoldBooksListSoldBooks.getSoldBooksList().remove(soldBooksListSoldBooks);
                    oldBooksIsbnOfSoldBooksListSoldBooks = em.merge(oldBooksIsbnOfSoldBooksListSoldBooks);
                }
            }
            for (Reviews reviewsListReviews : books.getReviewsList()) {
                Books oldIsbnOfReviewsListReviews = reviewsListReviews.getIsbn();
                reviewsListReviews.setIsbn(books);
                reviewsListReviews = em.merge(reviewsListReviews);
                if (oldIsbnOfReviewsListReviews != null) {
                    oldIsbnOfReviewsListReviews.getReviewsList().remove(reviewsListReviews);
                    oldIsbnOfReviewsListReviews = em.merge(oldIsbnOfReviewsListReviews);
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

    public void edit(Books books) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            LOG.debug("edit book title = " + books.getTitle());
            Books persistentBooks = em.find(Books.class, books.getIsbn());
            Authors authorIdOld = persistentBooks.getAuthorId();
            Authors authorIdNew = books.getAuthorId();
            Publishers publisherIdOld = persistentBooks.getPublisherId();
            Publishers publisherIdNew = books.getPublisherId();
            List<Format> formatListOld = persistentBooks.getFormatList();
            List<Format> formatListNew = books.getFormatList();
            List<SoldBooks> soldBooksListOld = persistentBooks.getSoldBooksList();
            List<SoldBooks> soldBooksListNew = books.getSoldBooksList();
            List<Reviews> reviewsListOld = persistentBooks.getReviewsList();
            List<Reviews> reviewsListNew = books.getReviewsList();
            List<String> illegalOrphanMessages = null;
            for (SoldBooks soldBooksListOldSoldBooks : soldBooksListOld) {
                if (!soldBooksListNew.contains(soldBooksListOldSoldBooks)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SoldBooks " + soldBooksListOldSoldBooks + " since its booksIsbn field is not nullable.");
                }
            }
            for (Reviews reviewsListOldReviews : reviewsListOld) {
                if (!reviewsListNew.contains(reviewsListOldReviews)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Reviews " + reviewsListOldReviews + " since its isbn field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (authorIdNew != null) {
                authorIdNew = em.getReference(authorIdNew.getClass(), authorIdNew.getId());
                books.setAuthorId(authorIdNew);
            }
            if (publisherIdNew != null) {
                publisherIdNew = em.getReference(publisherIdNew.getClass(), publisherIdNew.getId());
                books.setPublisherId(publisherIdNew);
            }
            List<Format> attachedFormatListNew = new ArrayList<Format>();
            for (Format formatListNewFormatToAttach : formatListNew) {
                formatListNewFormatToAttach = em.getReference(formatListNewFormatToAttach.getClass(), formatListNewFormatToAttach.getId());
                attachedFormatListNew.add(formatListNewFormatToAttach);
            }
            formatListNew = attachedFormatListNew;
            books.setFormatList(formatListNew);
            List<SoldBooks> attachedSoldBooksListNew = new ArrayList<SoldBooks>();
            for (SoldBooks soldBooksListNewSoldBooksToAttach : soldBooksListNew) {
                soldBooksListNewSoldBooksToAttach = em.getReference(soldBooksListNewSoldBooksToAttach.getClass(), soldBooksListNewSoldBooksToAttach.getId());
                attachedSoldBooksListNew.add(soldBooksListNewSoldBooksToAttach);
            }
            soldBooksListNew = attachedSoldBooksListNew;
            books.setSoldBooksList(soldBooksListNew);
            List<Reviews> attachedReviewsListNew = new ArrayList<Reviews>();
            for (Reviews reviewsListNewReviewsToAttach : reviewsListNew) {
                reviewsListNewReviewsToAttach = em.getReference(reviewsListNewReviewsToAttach.getClass(), reviewsListNewReviewsToAttach.getId());
                attachedReviewsListNew.add(reviewsListNewReviewsToAttach);
            }
            reviewsListNew = attachedReviewsListNew;
            books.setReviewsList(reviewsListNew);
            books = em.merge(books);
            if (authorIdOld != null && !authorIdOld.equals(authorIdNew)) {
                authorIdOld.getBooksList().remove(books);
                authorIdOld = em.merge(authorIdOld);
            }
            if (authorIdNew != null && !authorIdNew.equals(authorIdOld)) {
                authorIdNew.getBooksList().add(books);
                authorIdNew = em.merge(authorIdNew);
            }
            if (publisherIdOld != null && !publisherIdOld.equals(publisherIdNew)) {
                publisherIdOld.getBooksList().remove(books);
                publisherIdOld = em.merge(publisherIdOld);
            }
            if (publisherIdNew != null && !publisherIdNew.equals(publisherIdOld)) {
                publisherIdNew.getBooksList().add(books);
                publisherIdNew = em.merge(publisherIdNew);
            }
            for (Format formatListOldFormat : formatListOld) {
                if (!formatListNew.contains(formatListOldFormat)) {
                    formatListOldFormat.getBooksList().remove(books);
                    formatListOldFormat = em.merge(formatListOldFormat);
                }
            }
            for (Format formatListNewFormat : formatListNew) {
                if (!formatListOld.contains(formatListNewFormat)) {
                    formatListNewFormat.getBooksList().add(books);
                    formatListNewFormat = em.merge(formatListNewFormat);
                }
            }
            for (SoldBooks soldBooksListNewSoldBooks : soldBooksListNew) {
                if (!soldBooksListOld.contains(soldBooksListNewSoldBooks)) {
                    Books oldBooksIsbnOfSoldBooksListNewSoldBooks = soldBooksListNewSoldBooks.getBooksIsbn();
                    soldBooksListNewSoldBooks.setBooksIsbn(books);
                    soldBooksListNewSoldBooks = em.merge(soldBooksListNewSoldBooks);
                    if (oldBooksIsbnOfSoldBooksListNewSoldBooks != null && !oldBooksIsbnOfSoldBooksListNewSoldBooks.equals(books)) {
                        oldBooksIsbnOfSoldBooksListNewSoldBooks.getSoldBooksList().remove(soldBooksListNewSoldBooks);
                        oldBooksIsbnOfSoldBooksListNewSoldBooks = em.merge(oldBooksIsbnOfSoldBooksListNewSoldBooks);
                    }
                }
            }
            for (Reviews reviewsListNewReviews : reviewsListNew) {
                if (!reviewsListOld.contains(reviewsListNewReviews)) {
                    Books oldIsbnOfReviewsListNewReviews = reviewsListNewReviews.getIsbn();
                    reviewsListNewReviews.setIsbn(books);
                    reviewsListNewReviews = em.merge(reviewsListNewReviews);
                    if (oldIsbnOfReviewsListNewReviews != null && !oldIsbnOfReviewsListNewReviews.equals(books)) {
                        oldIsbnOfReviewsListNewReviews.getReviewsList().remove(reviewsListNewReviews);
                        oldIsbnOfReviewsListNewReviews = em.merge(oldIsbnOfReviewsListNewReviews);
                    }
                }
            }
            utx.commit();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "Book edited: " + books.getTitle()));
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Book not edited: " + books.getTitle()));
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = books.getIsbn();
                if (findBooks(id) == null) {
                    throw new NonexistentEntityException("The books with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Books books;
            try {
                books = em.getReference(Books.class, id);
                books.getIsbn();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The books with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<SoldBooks> soldBooksListOrphanCheck = books.getSoldBooksList();
            for (SoldBooks soldBooksListOrphanCheckSoldBooks : soldBooksListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Books (" + books + ") cannot be destroyed since the SoldBooks " + soldBooksListOrphanCheckSoldBooks + " in its soldBooksList field has a non-nullable booksIsbn field.");
            }
            List<Reviews> reviewsListOrphanCheck = books.getReviewsList();
            for (Reviews reviewsListOrphanCheckReviews : reviewsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Books (" + books + ") cannot be destroyed since the Reviews " + reviewsListOrphanCheckReviews + " in its reviewsList field has a non-nullable isbn field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Authors authorId = books.getAuthorId();
            if (authorId != null) {
                authorId.getBooksList().remove(books);
                authorId = em.merge(authorId);
            }
            Publishers publisherId = books.getPublisherId();
            if (publisherId != null) {
                publisherId.getBooksList().remove(books);
                publisherId = em.merge(publisherId);
            }
            List<Format> formatList = books.getFormatList();
            for (Format formatListFormat : formatList) {
                formatListFormat.getBooksList().remove(books);
                formatListFormat = em.merge(formatListFormat);
            }
            em.remove(books);
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Books> findBooksEntities() {
        return findBooksEntities(true, -1, -1);
    }

    public List<Books> findBooksEntities(int maxResults, int firstResult) {
        return findBooksEntities(false, maxResults, firstResult);
    }

    private List<Books> findBooksEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Books.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public List findBooks(String searchTerm) {
        List<Books> books = new ArrayList();
        books.addAll(findBooksWithIsbn(searchTerm));
        books.addAll(findBooksWithTitle(searchTerm));
        return books;
    }

    public int getBooksCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Books> rt = cq.from(Books.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    /**
     * JPA criteria query to find all the books within a genre
     * 
     * @param searchTerm representing the pattern that might correspond to a/some title, author name and/or isbn
     * @param selectBy representing in what category you are selecting you choice by (book title, author name, publisher or isbn)
     * @param genreName representing the name of a genre
     * @return book list that have a title that follows the pattern 
     * @author Alexander Berestetskyy
     */
    public List findBooksWithGenre(String searchTerm, String selectBy, String genreName){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Books> cq = cb.createQuery(Books.class);
        Root<Books> books = cq.from(Books.class);
        Join<Books,Genre> genre = books.join("genreList");
        Join<Books,Authors> author = books.join("authorId");
        Join<Books, Publishers> publisher = books.join("publisherId");
        ParameterExpression<String> g = cb.parameter(String.class);
        ParameterExpression<String> st = cb.parameter(String.class);
        if (selectBy.equalsIgnoreCase("bookTitle")){
            LOG.debug("case 1 with bookTitle");
            cq.select(books).where(cb.and(cb.equal(genre.get("genre"),g), cb.like(books.get("title"),st), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        } else if (selectBy.equalsIgnoreCase("authorName")){
            LOG.debug("case 2 with authorName");
            cq.select(books).where(cb.and(cb.equal(genre.get("genre"),g), cb.like(author.get("name"),st), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        } else if(selectBy.equalsIgnoreCase("publisher")){
            LOG.debug("case 3 with publisher");
            cq.select(books).where(cb.and(cb.equal(genre.get("genre"),g), cb.like(publisher.get("name"),st), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        } else if(selectBy.equalsIgnoreCase("isbn")){
            LOG.debug("case 4 with isbn");
            cq.select(books).where(cb.and(cb.equal(genre.get("genre"),g), cb.like(books.get("isbn"),st), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        }
        TypedQuery<Books> query = em.createQuery(cq);        
        query.setParameter(g, genreName);
        query.setParameter(st, "%"+searchTerm+"%");
        List<Books> booksList = query.getResultList();
        
        return booksList;
    }
   
    /**
     * JPA criteria query to find all the books within a genre and doesn't have a search term
     * 
     * @param genreName representing the name of a genre
     * @return book list that falls in that genre
     * @author Alexander Berestetskyy
     */
    public List findBooksWithGenreNoSearchTerm(String genreName){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Books> cq = cb.createQuery(Books.class);
        Root<Books> books = cq.from(Books.class);
        Join<Books,Genre> genre = books.join("genreList");
        ParameterExpression<String> g = cb.parameter(String.class);
        cq.select(books).where(cb.and(cb.equal(genre.get("genre"),g), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        TypedQuery<Books> query = em.createQuery(cq);        
        query.setParameter(g, genreName);
        List<Books> booksList = query.getResultList();
        return booksList;
    }

   /**
     * JPA criteria query to find all the books with a title that follows the pattern provided
     * as an argument
     * 
     * @param searchTerm representing the pattern that might correspond to a/some titles
     * @return book list that have a title that follows the pattern 
     * @author Alexander Berestetskyy
     */
    public List findBooksWithTitle(String searchTerm){
        
        //Search all the books that have corresponding titles
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Books> q = cb.createQuery(Books.class);
        Root<Books> books = q.from(Books.class);
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(books).where(cb.and(cb.like(books.get("title"), t), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        TypedQuery<Books> query = em.createQuery(q);
        query.setParameter(t, "%"+searchTerm+"%");
        List<Books> booksList = query.getResultList();
        
        return booksList;
    }


    /**
     * JPA criteria query to find all the books with an author name that follows the pattern provided
     * as an argument
     * 
     * @param searchTerm representing the pattern that might correspond to a/some author name
     * @return book list that have a title that follows the pattern 
     * @author Alexander Berestetskyy
     */
    public List findBooksWithAuthorName(String searchTerm){
        
        //Search all the books that have corresponding titles
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Books> q = cb.createQuery(Books.class);
        Root<Books> books = q.from(Books.class);
        //Do join with authors
        Join<Books, Authors> authors = books.join("authorId");
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(books).where(cb.and(cb.like(authors.get("name"), t), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        TypedQuery<Books> query = em.createQuery(q);
        query.setParameter(t, "%"+searchTerm+"%");
        List<Books> booksList = query.getResultList();
        return booksList;
    }


    /**
     * JPA criteria query to find all the books with a publisher that follows the pattern provided
     * as an argument
     * 
     * @param searchTerm representing the pattern that might correspond to a/some publishers
     * @return book list that have a title that follows the pattern 
     * @author Alexander Berestetskyy
     */
    public List findBooksWithPublisher(String searchTerm){
        
        //Search all the books that have corresponding titles
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Books> q = cb.createQuery(Books.class);
        Root<Books> books = q.from(Books.class);
        //Do join with publishers
        Join<Books, Publishers> publishers = books.join("publisherId");
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(books).where(cb.and(cb.like(publishers.get("name"), t), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        TypedQuery<Books> query = em.createQuery(q);
        query.setParameter(t, "%"+searchTerm+"%");
        List<Books> booksList = query.getResultList();
        return booksList;
    }


    /**
     * JPA criteria query to find all the books with an isbn that follows the pattern provided
     * as an argument
     * 
     * @param searchTerm representing the pattern that might correspond to a/some isbn
     * @return book list that have a title that follows the pattern 
     * @author Alexander Berestetskyy
     */
    public List findBooksWithIsbn(String searchTerm){
        
        //Search all the books that have corresponding titles
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Books> q = cb.createQuery(Books.class);
        Root<Books> books = q.from(Books.class);
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(books).where(cb.and(cb.like(books.get("isbn"), t), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        TypedQuery<Books> query = em.createQuery(q);
        query.setParameter(t, "%"+searchTerm+"%");
        List<Books> booksList = query.getResultList();
        return booksList;
    }


    /**
     * JPA criteria query to find all the genres and the number of books to that 
     * corresponding genre and that have a title that follows the pattern of the argument
     * 
     * @param title representing the pattern that might correspond to a/some titles
     * @return genreMap with the genre name and the number of books in that genre
     * @author Alexander Berestetskyy
     */
    public HashMap findGenresWithTitle(String title){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> q = cb.createQuery(Tuple.class);
        Root<Books> books = q.from(Books.class);
        Join<Books,Genre> genre = books.join("genreList");
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.multiselect(genre.get("genre"), cb.count(books.get("title")));
        q.where(cb.and(cb.like(books.get("title"), t), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        q.groupBy(genre.get("genre"));
        TypedQuery<Tuple> query = em.createQuery(q);
        query.setParameter(t, "%"+title+"%");
        List<Tuple> genreList = query.getResultList();
        HashMap<String,Long> genreWithCount = new HashMap<String,Long>();
        for ( Tuple tuple : genreList ) {
            String genreName = (String)tuple.get(0);
            Long count = (Long)tuple.get(1);
            genreWithCount.put(genreName, count);
        }
        return genreWithCount;
    }


    /**
     * JPA criteria query to find all the genres and the number of books to that 
     * corresponding genre and that have an author name that follows the pattern of the argument
     * 
     * @param searchTerm representing the pattern that might correspond to a/some author name
     * @return genreMap with the genre name and the number of books in that genre
     * @author Alexander Berestetskyy
     */
    public HashMap findGenresWithAuthorName(String searchTerm){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> q = cb.createQuery(Tuple.class);
        Root<Books> books = q.from(Books.class);
        //Do join with authors
        Join<Books, Authors> authors = books.join("authorId");
        Join<Books, Genre> genre = books.join("genreList");
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.multiselect(genre.get("genre"), cb.count(books.get("title")));
        q.where(cb.and(cb.like(authors.get("name"), t), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        q.groupBy(genre.get("genre"));
        TypedQuery<Tuple> query = em.createQuery(q);
        query.setParameter(t, "%"+searchTerm+"%");
        List<Tuple> genreList = query.getResultList();
        HashMap<String,Long> genreWithCount = new HashMap<String,Long>();
        for ( Tuple tuple : genreList ) {
            String genreName = (String)tuple.get(0);
            Long count = (Long)tuple.get(1);
            genreWithCount.put(genreName, count);
        }
        return genreWithCount;
    }


    /**
     * JPA criteria query to find all the genres and the number of books to that 
     * corresponding genre and that have a publisher that follows the pattern of the argument
     * 
     * @param searchTerm representing the pattern that might correspond to a/some publisher
     * @return genreMap with the genre name and the number of books in that genre
     * @author Alexander Berestetskyy
     */
    public HashMap findGenresWithPublisher(String searchTerm){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> q = cb.createQuery(Tuple.class);
        Root<Books> books = q.from(Books.class);
        //Do join with publishers
        Join<Books, Publishers> publishers = books.join("publisherId");
        Join<Books, Genre> genre = books.join("genreList");
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.multiselect(genre.get("genre"), cb.count(books.get("title")));
        q.where(cb.and(cb.like(publishers.get("name"), t), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        q.groupBy(genre.get("genre"));
        TypedQuery<Tuple> query = em.createQuery(q);
        query.setParameter(t, "%"+searchTerm+"%");
        List<Tuple> genreList = query.getResultList();
        HashMap<String,Long> genreWithCount = new HashMap<String,Long>();
        for ( Tuple tuple : genreList ) {
            String genreName = (String)tuple.get(0);
            Long count = (Long)tuple.get(1);
            genreWithCount.put(genreName, count);
        }
        return genreWithCount;
    }


    /**
     * JPA criteria query to find all the genres and the number of books to that 
     * corresponding genre and that have an isbn that follows the pattern of the argument
     * 
     * @param searchTerm representing the pattern that might correspond to a/some isbn
     * @return genreMap with the genre name and the number of books in that genre
     * @author Alexander Berestetskyy
     */
    public HashMap findGenresWithIsbn(String searchTerm){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> q = cb.createQuery(Tuple.class);
        Root<Books> books = q.from(Books.class);
        Join<Books,Genre> genre = books.join("genreList");
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.multiselect(genre.get("genre"), cb.count(books.get("title")));
        q.where(cb.and(cb.like(books.get("isbn"), t), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        q.groupBy(genre.get("genre"));
        TypedQuery<Tuple> query = em.createQuery(q);
        query.setParameter(t, "%"+searchTerm+"%");
        List<Tuple> genreList = query.getResultList();
        HashMap<String,Long> genreWithCount = new HashMap<String,Long>();
        for ( Tuple tuple : genreList ) {
            String genreName = (String)tuple.get(0);
            Long count = (Long)tuple.get(1);
            genreWithCount.put(genreName, count);
        }
        return genreWithCount;
    }


    /**
     * JPA criteria query to find six recommendation based on the parameters of a book.
     * Either three books from the same author and three books from the same genre, 
     * or six books from the same genre
     * 
     * @param anAuthorName representing the name of an author
     * @param aGenre representing the genre of the book
     * @param anIsbn representing the unique isbn of the book 
     * @return genreMap with the genre name and the number of books in that genre
     * @author Alexander Berestetskyy
     */
    public List findAllSixRecommendedBooks(String anAuthorName, Genre aGenre, String anIsbn){
        List<Books> recBooks;
        //attempting to retrieve 3 books from the same author, if there are any
            
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Books> q = cb.createQuery(Books.class);
        Root<Books> books = q.from(Books.class);
        Join<Books, Authors> authors = books.join("authorId");
        ParameterExpression<String> authorName = cb.parameter(String.class);
        ParameterExpression<String> isbn = cb.parameter(String.class);
        q.select(books);
        q.where(cb.and(cb.equal(authors.get("name"), authorName), cb.notEqual(books.get("isbn"), isbn), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        TypedQuery<Books> query1 = em.createQuery(q).setMaxResults(3);
        query1.setParameter(authorName, anAuthorName);
        query1.setParameter(isbn, anIsbn);
        recBooks = query1.getResultList();
        switch (recBooks.size()) {
            case 3:
                {
                    //retrieve 3 books of the same genre, there are already 3 books found from the same author
                    LOG.debug("case 1 for recommended books");
                    CriteriaBuilder cb1 = em.getCriteriaBuilder();
                    CriteriaQuery<Books> q1 = cb1.createQuery(Books.class);
                    Root<Books> firstbooks = q1.from(Books.class);
                    Join<Books,Authors> firstauthors = firstbooks.join("authorId");
                    Join<Books,Genre> firstgenre = firstbooks.join("genreList");
                    ParameterExpression<String> firstignoreauthor = cb1.parameter(String.class);
                    ParameterExpression<String> firstignoreisbn = cb1.parameter(String.class);
                    ParameterExpression<String> firstgenrematch = cb1.parameter(String.class);
                    q1.select(firstbooks);
                    q1.where(cb1.and(cb1.equal(firstgenre.get("genre"),firstgenrematch), cb1.notEqual(firstauthors.get("name"), firstignoreauthor), cb1.notEqual(firstbooks.get("isbn"), firstignoreisbn), cb1.equal(firstbooks.get("removalStatus"), Boolean.FALSE)));
                    TypedQuery<Books> firstquery = em.createQuery(q1).setMaxResults(3);
                    firstquery.setParameter(firstgenrematch, aGenre.getGenre());
                    firstquery.setParameter(firstignoreauthor, anAuthorName);
                    firstquery.setParameter(firstignoreisbn,anIsbn);
                    List<Books> bookInGenre = firstquery.getResultList();
                    for(Books book : bookInGenre){
                        recBooks.add(book);
                    }       
                    break;
                }
            case 2:
                {
                    //retrieve 4 books of the same genre, there are already 2 books found from the same author
                    LOG.debug("case 2 for recommended books");
                    CriteriaBuilder cb2 = em.getCriteriaBuilder();
                    CriteriaQuery<Books> q2 = cb2.createQuery(Books.class);
                    Root<Books> secondbooks = q2.from(Books.class);
                    Join<Books,Authors> secondauthors = secondbooks.join("authorId");
                    Join<Books,Genre> secondgenre = secondbooks.join("genreList");
                    ParameterExpression<String> secondignoreauthor = cb2.parameter(String.class);
                    ParameterExpression<String> secondignoreisbn = cb2.parameter(String.class);
                    ParameterExpression<String> secondgenrematch = cb2.parameter(String.class);
                    q2.select(secondbooks);
                    q2.where(cb2.and(cb2.equal(secondgenre.get("genre"), secondgenrematch), cb2.notEqual(secondauthors.get("name"), secondignoreauthor), cb2.notEqual(secondbooks.get("isbn"), secondignoreisbn), cb2.equal(secondbooks.get("removalStatus"), Boolean.FALSE)));
                    TypedQuery<Books> secondquery = em.createQuery(q2).setMaxResults(4);
                    secondquery.setParameter(secondgenrematch, aGenre.getGenre());
                    secondquery.setParameter(secondignoreauthor, anAuthorName);
                    secondquery.setParameter(secondignoreisbn, anIsbn);
                    List<Books> bookInGenre = secondquery.getResultList();
                    for(Books book : bookInGenre){
                        recBooks.add(book);
                    }       
                    break;
                }
            case 1:
                {
                    //retrieve 5 books of the same genre, there is already 1 book found from the same author
                    LOG.debug("case 3 for recommended books");
                    CriteriaBuilder cb3 = em.getCriteriaBuilder();
                    CriteriaQuery<Books> q3 = cb3.createQuery(Books.class);
                    Root<Books> thirdbooks = q3.from(Books.class);
                    Join<Books,Authors> thirdauthors = thirdbooks.join("authorId");
                    Join<Books,Genre> thirdgenre = thirdbooks.join("genreList");
                    ParameterExpression<String> thirdignoreauthor = cb3.parameter(String.class);
                    ParameterExpression<String> thirdignoreisbn = cb3.parameter(String.class);
                    ParameterExpression<String> thirdgenrematch = cb3.parameter(String.class);
                    q3.select(thirdbooks);
                    q3.where(cb3.and(cb3.equal(thirdgenre.get("genre"), thirdgenrematch), cb3.notEqual(thirdauthors.get("name"), thirdignoreauthor), cb3.notEqual(thirdbooks.get("isbn"), thirdignoreisbn), cb3.equal(thirdbooks.get("removalStatus"), Boolean.FALSE)));
                    TypedQuery<Books> thirdquery = em.createQuery(q3).setMaxResults(5);
                    thirdquery.setParameter(thirdgenrematch, aGenre.getGenre());
                    thirdquery.setParameter(thirdignoreauthor, anAuthorName);
                    thirdquery.setParameter(thirdignoreisbn, anIsbn);
                    List<Books> bookInGenre = thirdquery.getResultList();
                    for(Books book : bookInGenre){
                        recBooks.add(book);
                    }       
                    break;
                }
            default:
                {
                    //retrieve six books from the same genre
                    LOG.debug("case 4 for recommended books");
                    CriteriaBuilder cb4 = em.getCriteriaBuilder();
                    CriteriaQuery<Books> q4 = cb4.createQuery(Books.class);
                    Root<Books> fourthbooks = q4.from(Books.class);
                    Join<Books,Authors> fourthauthors = fourthbooks.join("authorId");
                    Join<Books,Genre> fourthgenre = fourthbooks.join("genreList");
                    ParameterExpression<String> fourthignoreauthor = cb4.parameter(String.class);
                    ParameterExpression<String> fourthignoreisbn = cb4.parameter(String.class);
                    ParameterExpression<String> fourthgenrematch = cb4.parameter(String.class);
                    q4.select(fourthbooks);
                    q4.where(cb4.and(cb4.equal(fourthgenre.get("genre"),fourthgenrematch), cb4.notEqual(fourthauthors.get("name"), fourthignoreauthor), cb4.notEqual(fourthbooks.get("isbn"),fourthignoreisbn), cb4.equal(fourthbooks.get("removalStatus"), Boolean.FALSE)));
                    TypedQuery<Books> fourthquery = em.createQuery(q4).setMaxResults(6);
                    fourthquery.setParameter(fourthgenrematch, aGenre.getGenre());
                    fourthquery.setParameter(fourthignoreauthor, anAuthorName);
                    fourthquery.setParameter(fourthignoreisbn, anIsbn);
                    List<Books> bookInGenre = fourthquery.getResultList();
                    for(Books book : bookInGenre){
                        recBooks.add(book);
                    }       break;
                }
        }
        return recBooks;
    }

    /**
     * Takes as input a book ISBN and a discount percentage Sets the book sale
     * price to have the input discount as well as the discount field is updated
     *
     * @author Hadil Elhashani
     * @param isbn
     * @param discountPercent
     * @return
     */
    public Books editBookPrice(String isbn, long discountPercent) {
        LOG.debug("editBookPrice");
        try {
            utx.begin();
        } catch (NotSupportedException | SystemException ex) {
            java.util.logging.Logger.getLogger(BooksJpaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Books persistentBooks = em.find(Books.class, isbn);

        if(validateDiscount(discountPercent)){
            BigDecimal discount = new BigDecimal(discountPercent);
            persistentBooks.setDiscount(discount.longValue());
            discount = new BigDecimal(1).subtract(discount.divide(new BigDecimal(100)));
            BigDecimal oldSalePrice = persistentBooks.getListPrice();
            BigDecimal newSalePrice = oldSalePrice.multiply(discount).setScale(2, RoundingMode.HALF_EVEN);
            
            if(persistentBooks.getWholesalePrice().compareTo(newSalePrice) >= 0){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, 
                        "Error", "Sale price must be larger than wholeSalePrice: " + newSalePrice + " is less than " + persistentBooks.getWholesalePrice()));
            }
            else{
                persistentBooks.setSalePrice(newSalePrice);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                        "Successful", "Book sale price edited: " + persistentBooks.getTitle()));
            }

            LOG.debug("Sale price of book with isbn= " + isbn + " is updated to " + newSalePrice);
            LOG.debug("Discount of book with isbn= " + isbn + " is updated to " + discount);


            try {
                utx.commit();

            } catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException | SystemException ex) {
                java.util.logging.Logger.getLogger(BooksJpaController.class.getName()).log(Level.SEVERE, null, ex);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "discount is not between 1 and 99"));
            }
        }

        return persistentBooks;
    }
    
    /**
     * 
     * @param discount
     * @return 
     */
    private boolean validateDiscount(long discount){
        if(discount > 100 || discount < 0){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Discount must be between 1 and 99"));
            return false;
        }
        return true;
    }

    /**
     * Removes discount from a book and sets the sales price to be equal to the
     * list price
     *
     * @author Hadil Elhashani
     * @param isbn
     * @return
     */
    public Books removeBookDiscount(String isbn) {
        LOG.debug("removeBookDiscount");
        try {
            utx.begin();
        } catch (NotSupportedException | SystemException ex) {
            java.util.logging.Logger.getLogger(BooksJpaController.class.getName()).log(Level.SEVERE, null, ex);
        }

        Books persistentBooks = em.find(Books.class, isbn);
        persistentBooks.setDiscount(0);
        persistentBooks.setSalePrice(persistentBooks.getListPrice());

        try {
            utx.commit();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "Sale removed: " + persistentBooks.getTitle()));
        } catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException | SystemException ex) {
            java.util.logging.Logger.getLogger(BooksJpaController.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Sale not removed:" + persistentBooks.getTitle()));
        }
        LOG.debug("Discount removed from book isbn = " + isbn);
        return persistentBooks;
    }

    /**
     * Updates the removal status of a book
     *
     * @author Hadil Elhashani
     * @param isbn
     * @param status
     * @return
     */
    public Books updateBookRemovalStatus(String isbn, boolean status) {
        LOG.debug("updateBookRemovalStatus");
        try {
            utx.begin();
        } catch (NotSupportedException | SystemException ex) {
            java.util.logging.Logger.getLogger(BooksJpaController.class.getName()).log(Level.SEVERE, null, ex);
        }

        Books persistentBooks = em.find(Books.class, isbn);
        persistentBooks.setRemovalStatus(status);

        try {
            utx.commit();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "Book removed: " + persistentBooks.getTitle()));
        } catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException | SystemException ex) {
            java.util.logging.Logger.getLogger(BooksJpaController.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Book not removed: " + persistentBooks.getTitle()));
        }
        LOG.debug("Removal status of book isbn= " + isbn + " is updated");
        return persistentBooks;
    }

    /**
     * JPA criteria query to find top 5 sellers in a genre, and the all the remaining books in that genre
     * 
     * @param selectByGenre representing the genre that will be used to retrieve the books
     * @return book list that have the top 5 sellers and remaining books in that genre
     * @author Alexander Berestetskyy
     */
    public List findAllBooksInGenreBestSeller(String selectByGenre){
        List<Books> topSellerBooks = new ArrayList<Books>();
        //Retrieve top 5 sellers first in a Tuple by retrieving the isbn of the book with the count
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> q = cb.createQuery(Tuple.class);
        Root<SoldBooks> soldBooks = q.from(SoldBooks.class);
        Join<SoldBooks,Books> books = soldBooks.join("booksIsbn");
        Join<Books,Genre> genre = books.join("genreList");
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.multiselect(books.get("isbn"),cb.count(soldBooks.get("booksIsbn")));
        q.where(cb.and(cb.equal(genre.get("genre"), t), cb.equal(books.get("removalStatus"), Boolean.FALSE)));
        q.groupBy(books.get("isbn"));
        q.orderBy(cb.desc(cb.count(soldBooks.get("booksIsbn"))));
        TypedQuery<Tuple> query = em.createQuery(q).setMaxResults(5);
        query.setParameter(t, selectByGenre);
        List<Tuple> topSellersTuple = query.getResultList();
        
        //retrieve the top 5 sellers from the Tuple by going through its list 
        for(Tuple bestSellerTitle : topSellersTuple){
            String isbn = (String)bestSellerTitle.get(0); 
            Long timesBookSold = (Long)bestSellerTitle.get(1);
            CriteriaBuilder cb1 = em.getCriteriaBuilder();
            CriteriaQuery<Books> q1 = cb1.createQuery(Books.class);
            Root<Books> sellerBook = q1.from(Books.class);
            ParameterExpression<String> t1 = cb1.parameter(String.class);
            q1.select(sellerBook).where(cb1.equal(sellerBook.get("isbn"),t1));
            TypedQuery<Books> query2 = em.createQuery(q1);
            query2.setParameter(t1, isbn);
            Books oneSellerBook = query2.getSingleResult();
            topSellerBooks.add(oneSellerBook);
        }
        
        List<Books> allBooksInGenre = topSellerBooks;
           
        //Retrieve all remaining books in that genre
        CriteriaBuilder cb2 = em.getCriteriaBuilder();
        CriteriaQuery<Books> q2 = cb2.createQuery(Books.class);
        Root<Books> remainBooks = q2.from(Books.class);
        Join<Books,Genre> genreBooksRemain = remainBooks.join("genreList");
        ParameterExpression<String> t2 = cb2.parameter(String.class);
        q2.select(remainBooks).where(cb2.and(cb2.equal(genreBooksRemain.get("genre"),t2), cb2.equal(remainBooks.get("removalStatus"), Boolean.FALSE)));
        q2.orderBy(cb.asc(remainBooks.get("title")));
        TypedQuery<Books> query2 = em.createQuery(q2);
        query2.setParameter(t2, selectByGenre);
        List<Books> remainingBooks = query2.getResultList();
        
        int counter=0;
        for(Books singleBook : remainingBooks){
            for(Books sellerBook : topSellerBooks){
                if(singleBook.getTitle().equals(sellerBook.getTitle())){
                    counter++;
                }
            }
            if(counter == 0){
                //means that book isn't in top 5 sellers in that genre
                allBooksInGenre.add(singleBook);
            }
            counter=0;
        }
        
        return allBooksInGenre;
    }


    /**
     * Converts input string to date
     *
     * @author Hadil Elhashani
     * @param dateStr
     * @return
     */
    private Date convertToDate(String dateStr) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        try {
            date = df.parse(dateStr);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(BooksJpaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOG.debug("DATE= " + date.toString());
        return date;
    }

    /**
     * The method checks if a book does not exist in the sols_books table, which
     * means no client has purchased it
     *
     * @author Hadil Elhashani
     * @param fromDate
     * @param toDate
     * @return
     */
    public List findZeroSalesBook(String fromDate, String toDate) {
        LOG.debug("findZeroSalesBook");

        

        List<Books> booksList = this.findBooksEntities();
        
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Sales> q = cb.createQuery(Sales.class);
        
        Root<Sales> sales = q.from(Sales.class);     
        
        Date dateTo = convertToDate(toDate + " 23:59:59");
        Date dateFrom = convertToDate(fromDate + " 00:00:00");

        Predicate between = cb.between(sales.get("date"), dateFrom, dateTo);
        q.select(sales).where(between);
        TypedQuery<Sales> query = em.createQuery(q);        
        List<Sales> salesList = query.getResultList();
        
        List<String> soldBooksISBNList = new ArrayList<>();
        
        for(Sales s : salesList) {
            List<SoldBooks> sbList = s.getSoldBooksList();
            
            for (SoldBooks sb: sbList) {
                String isbn = sb.getBooksIsbn().getIsbn();
                if (soldBooksISBNList.contains(isbn)) {
                    continue;
                } else {
                    soldBooksISBNList.add(isbn);
                }
            }            
        }
        
        
        List<Books> zeroBooksList = new ArrayList<>();
        
        for(Books b :booksList) {
            if (!soldBooksISBNList.contains(b.getIsbn())) {
                zeroBooksList.add(b);
            } else {
                soldBooksISBNList.remove(b.getIsbn());
            }
        }
        
        
        return zeroBooksList;
    }

    /**
     * The method returns a list of type TopSeller that is in descending order
     * based on the number of times it was purchased
     *
     * @author Hadil Elhashani
     * @param fromDate
     * @param toDate
     * @return
     */
    public List findTopSeller(String fromDate, String toDate) {
        LOG.debug("findTopSeller");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<TopSeller> q = cb.createQuery(TopSeller.class);
        Root<Books> books = q.from(Books.class);
        //joining with the sold_books table using soldBooksList
        Join<Books, SoldBooks> soldbooks = books.join("soldBooksList");
        Join<Sales, SoldBooks> sales = soldbooks.join("salesId");

        //converting strings to dates
        Date dateTo = convertToDate(toDate + " 23:59:59");
        Date dateFrom = convertToDate(fromDate + " 00:00:00");

        Predicate between = cb.between(sales.get("date"), dateFrom, dateTo);

        //Constructing TopSeller list with properties from Books class
        q.select(cb.construct(TopSeller.class,
                books.get("isbn"), books.get("imageUrl"),
                books.get("title"), books.get("listPrice"),
                books.get("wholesalePrice"),
                cb.sum(soldbooks.get("price"))))
                .groupBy(books.get("isbn"))
                .orderBy(cb.desc(cb.sum(soldbooks.get("price"))))
                .where(cb.and(between, cb.equal(sales.get("isRemoved"), Boolean.FALSE)));

        TypedQuery<TopSeller> query = em.createQuery(q);
        List<TopSeller> topSellerList = query.getResultList();
        //LOG.debug("Top seller book is : " + topSellerList.get(0).getBookTitle() + " count= " + topSellerList.get(0).getCount());
        return topSellerList;
    }

    /**
     * The method returns a list of type TopClient in descending order that
     * contains the user's information along with the total amount of books they
     * have purchased
     *
     * @author Hadil Elhashani
     * @param fromDate
     * @param toDate
     * @return
     */
    public List findTopClients(String fromDate, String toDate) {
        LOG.debug("findTopClients");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<TopClient> q = cb.createQuery(TopClient.class);
        Root<Books> books = q.from(Books.class);

        //joining tables
        Join<Books, SoldBooks> soldbooks = books.join("soldBooksList");
        Join<Sales, SoldBooks> sales = soldbooks.join("salesId");
        Join<Sales, Users> users = sales.join("usersId");

        //converting strings to dates
        Date dateTo = convertToDate(toDate + " 23:59:59");
        Date dateFrom = convertToDate(fromDate + " 00:00:00");

        Predicate between = cb.between(sales.get("date"), dateFrom, dateTo);

        //Constructing TopClient list with properties from User class
        q.select(cb.construct(TopClient.class,
                users.get("id"), users.get("firstName"),
                users.get("lastName"), users.get("email"),
                cb.sum(soldbooks.get("price"))))
                .groupBy(users.get("id"))
                .orderBy(cb.desc(cb.sum(soldbooks.get("price"))))
                .where(cb.and(between, cb.equal(sales.get("isRemoved"), Boolean.FALSE)));

        TypedQuery<TopClient> query = em.createQuery(q);
        List<TopClient> topClientList = query.getResultList();
        return topClientList;
    }
    
    
    
    public List<Books> findRecentBooks() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Books> q = cb.createQuery(Books.class);
        
        Root<Books> books = q.from(Books.class);
        q.select(books).where(cb.equal(books.get("removalStatus"), Boolean.FALSE)).orderBy(cb.desc(books.get("dateEntered"))).orderBy(cb.desc(books.get("dateEntered")));
        
        TypedQuery<Books> query = em.createQuery(q).setMaxResults(10);
        
        
        List<Books> booksList = query.getResultList();
        return booksList;
    }
    
    public List<Books> findRecentBooksOnSale() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Books> q = cb.createQuery(Books.class);
        
        Root<Books> books = q.from(Books.class);
        
        q.select(books).where( cb.and(cb.equal(books.get("removalStatus"), Boolean.FALSE), cb.greaterThan(books.get("discount"),0)));
        TypedQuery<Books> query = em.createQuery(q).setMaxResults(10);
        
        
        List<Books> booksList = query.getResultList();
        return booksList;
    }
    

    /**
     * The method returns a list of type AuthorSale in descending order that
     * consists of the author's name and the total amount of books prices that
     * they have written
     *
     * @author Hadil Elhashani
     * @param fromDate
     * @param toDate
     * @param name
     * @return
     */
    public List findSalesByAuthor(String fromDate, String toDate, String name) {
        LOG.debug("findSalesByAuthor");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AuthorSale> q = cb.createQuery(AuthorSale.class);
        Root<Books> books = q.from(Books.class);

        LOG.debug("author name = " + name);

        //joinning the tables
        Join<Books, Authors> authors = books.join("authorId");
        Join<Books, SoldBooks> soldbooks = books.join("soldBooksList");
        Join<Sales, SoldBooks> sales = soldbooks.join("salesId");

        //converting strings to dates
        Date dateTo = convertToDate(toDate + " 23:59:59");
        Date dateFrom = convertToDate(fromDate + " 00:00:00");

        Predicate between = cb.between(sales.get("date"), dateFrom, dateTo);

        //Constructing AuthorSale list with properties from Author class
        q.select(cb.construct(AuthorSale.class,
                authors.get("name"), books.get("title"),
                soldbooks.get("price"), sales.get("date")))
                .orderBy(cb.asc(sales.get("date")))
                .where(cb.and(between, cb.equal(authors.get("name"), name),
                        cb.equal(sales.get("isRemoved"), Boolean.FALSE)));

        TypedQuery<AuthorSale> query = em.createQuery(q);
        List<AuthorSale> authorsList = query.getResultList();
        return authorsList;
    }

    /**
     * The method returns a list of type PublisherSale in descending order that
     * consists of the publisher's name and the total amount of books prices
     * that they published
     *
     * @author Hadil Elhashani
     * @param fromDate
     * @param toDate
     * @return
     */
    public List findSalesByPublisher(String fromDate, String toDate, String name) {
        LOG.debug("findSalesByPublisher");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<PublisherSale> q = cb.createQuery(PublisherSale.class);
        Root<Books> books = q.from(Books.class);

        LOG.debug("publisher name = " + name);

        Join<Books, Publishers> publishers = books.join("publisherId");
        Join<Books, SoldBooks> soldbooks = books.join("soldBooksList");
        Join<Sales, SoldBooks> sales = soldbooks.join("salesId");

        //converting strings to dates
        Date dateTo = convertToDate(toDate + " 23:59:59");
        Date dateFrom = convertToDate(fromDate + " 00:00:00");

        Predicate between = cb.between(sales.get("date"), dateFrom, dateTo);

        //Constructing PublisherSale list with properties from Publisher class
        q.select(cb.construct(PublisherSale.class,
                publishers.get("name"), books.get("title"),
                soldbooks.get("price"), sales.get("date")))
                .orderBy(cb.asc(sales.get("date")))
                .where(cb.and(between, cb.equal(publishers.get("name"), name),
                        cb.equal(sales.get("isRemoved"), Boolean.FALSE)));

        TypedQuery<PublisherSale> query = em.createQuery(q);
        List<PublisherSale> publishersList = query.getResultList();
        //LOG.debug("Top publisher is : " + publishersList.get(0).getPublisherName() + " count= " + publishersList.get(0).getTotalSales());
        return publishersList;
    }

    /**
     *
     * @author Hadil Elhashani
     * @param fromDate
     * @param toDate
     * @param email
     * @return
     */
    public List findSalesByClient(String fromDate, String toDate, String email) {
        LOG.debug("findSalesByClient");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<ClientSale> q = cb.createQuery(ClientSale.class);
        Root<Books> books = q.from(Books.class);

        LOG.debug("client email = " + email);

        Join<Books, SoldBooks> soldbooks = books.join("soldBooksList");
        Join<Sales, SoldBooks> sales = soldbooks.join("salesId");
        Join<Sales, Users> users = sales.join("usersId");

        //converting strings to dates
        Date dateTo = convertToDate(toDate + " 23:59:59");
        Date dateFrom = convertToDate(fromDate + " 00:00:00");

        Predicate between = cb.between(sales.get("date"), dateFrom, dateTo);

        //Constructing ClientSale list with properties from User and Book class
        q.select(cb.construct(ClientSale.class,
                users.get("firstName"), users.get("lastName"),
                books.get("title"), soldbooks.get("price"),
                sales.get("date")))
                .orderBy(cb.asc(sales.get("date")))
                .where(cb.and(between, cb.equal(users.get("email"), email),
                        cb.equal(sales.get("isRemoved"), Boolean.FALSE)));

        TypedQuery<ClientSale> query = em.createQuery(q);
        List<ClientSale> clientssList = query.getResultList();
        return clientssList;
    }

    /**
     *
     * @author Hadil Elhashani
     * @param fromDate
     * @param toDate
     * @return
     */
    public List findTotalSales(String fromDate, String toDate) {
        LOG.debug("findTotalSales");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Inventory> q = cb.createQuery(Inventory.class);
        Root<Books> books = q.from(Books.class);

        Join<Books, SoldBooks> soldbooks = books.join("soldBooksList");
        Join<Sales, SoldBooks> sales = soldbooks.join("salesId");

        //converting strings to dates
        Date dateTo = convertToDate(toDate + " 23:59:59");
        Date dateFrom = convertToDate(fromDate + " 00:00:00");

        Predicate between = cb.between(sales.get("date"), dateFrom, dateTo);

        //Constructing Inventory list with properties from book class
        q.select(cb.construct(Inventory.class,
                books.get("title"), books.get("imageUrl"),
                cb.sum(soldbooks.get("price"))))
                .groupBy(books.get("title"), books.get("imageUrl"))
                .orderBy(cb.desc(cb.sum(soldbooks.get("price"))))
                .where(cb.and(between), cb.equal(sales.get("isRemoved"), Boolean.FALSE));

        TypedQuery<Inventory> query = em.createQuery(q);
        List<Inventory> list = query.getResultList();
        return list;
    }

    /**
     *
     * @author Hadil Elhashani
     * @param fromDate
     * @param toDate
     * @return
     */
    public List findStockReport(String fromDate, String toDate) {
        LOG.debug("findTotalSales");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Inventory> q = cb.createQuery(Inventory.class);
        Root<Books> books = q.from(Books.class);

        Join<Books, SoldBooks> soldbooks = books.join("soldBooksList");
        Join<Sales, SoldBooks> sales = soldbooks.join("salesId");

        //converting strings to dates
        Date dateTo = convertToDate(toDate + " 23:59:59");
        Date dateFrom = convertToDate(fromDate + " 00:00:00");

        Predicate between = cb.between(sales.get("date"), dateFrom, dateTo);

        //Constructing Inventory list with properties from book class
        q.select(cb.construct(Inventory.class,
                books.get("title"), books.get("imageUrl"),
                books.get("listPrice"), books.get("wholesalePrice"),
                cb.count(books)))
                .groupBy(books.get("title"), books.get("imageUrl"),
                        books.get("listPrice"), books.get("wholesalePrice"))
                .orderBy(cb.desc(cb.count(books)))
                .where(cb.and(between), cb.equal(sales.get("isRemoved"), Boolean.FALSE));

        TypedQuery<Inventory> query = em.createQuery(q);
        List<Inventory> list = query.getResultList();
        return list;
    }
    
    /**
    * JPA criteria query to find the number of reviews for a specific book
    * 
    * @param isbn representing the isbn of a book
    * @return long that represents the number of reviews
    * @author Alexander Berestetskyy
    */
    public Long findNumReviewsOfSingleBook(String isbn){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> q = cb.createQuery(Long.class);
        Root<Reviews> reviews = q.from(Reviews.class);
        Join<Reviews, Books> books = reviews.join("isbn");
        ParameterExpression<String> t = cb.parameter(String.class);
        q.select(cb.count(reviews.get("id"))).where(cb.and(cb.equal(books.get("isbn"), t), cb.equal(reviews.get("isApproved"),"true")));
        TypedQuery<Long> query = em.createQuery(q);
        query.setParameter(t, isbn);
        Long num = query.getSingleResult();
        return num;
    }
    
    /**
    * JPA criteria query to find the average rating of a single book out of all the
    * approved reviews
    * 
    * @param isbn representing the isbn of a book
    * @return long that represents the number of reviews
    * @author Alexander Berestetskyy
    */
    public Double findAvgRatingOfSingleBook(String isbn){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Double> q = cb.createQuery(Double.class);
        Root<Reviews> reviews = q.from(Reviews.class);
        Join<Reviews, Books> books = reviews.join("isbn");
        ParameterExpression<String> t = cb.parameter(String.class);
        q.select(cb.avg(reviews.get("rating"))).where(cb.and(cb.equal(books.get("isbn"), t), cb.equal(reviews.get("isApproved"), "true")));
        TypedQuery<Double> query = em.createQuery(q);
        query.setParameter(t, isbn);
        Double avgRating = query.getSingleResult();
        return avgRating;
    }
    
    /**
    * JPA criteria query to find all the approved reviews for a single book
    * 
    * @param isbn representing the isbn of a book
    * @return long that represents the number of reviews
    * @author Alexander Berestetskyy
    */
    public List<Reviews> getApprovedReviewsOfBook(String isbn){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Reviews> q = cb.createQuery(Reviews.class);
        Root<Reviews> reviews = q.from(Reviews.class);
        Join<Reviews, Books> books = reviews.join("isbn");
        ParameterExpression<String> t = cb.parameter(String.class);
        q.select(reviews).where(cb.and(cb.equal(books.get("isbn"), t), cb.equal(reviews.get("isApproved"), "true")));
        TypedQuery<Reviews> query = em.createQuery(q);
        query.setParameter(t, isbn);
        List<Reviews> approvedReviews = query.getResultList(); 
        return approvedReviews;
    }
    
    /**
     * Return a list of all genres to show on menu
     * 
     * @return list of all genres
     * @author Alexander Berestetskyy
     */
    public List<Genre> retrieveAllGenres(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Genre> q = cb.createQuery(Genre.class);
        Root<Genre> genre = q.from(Genre.class);
        q.select(genre);
        TypedQuery<Genre> query = em.createQuery(q);
        List<Genre> genreList = query.getResultList();
        return genreList;
    }
    
    /**
     * Find a single Books object based on the isbn passed as an argument
     * 
     * @param anIsbn isbn of a book
     * @return Books object based on the isbn argument
     * @author Alexander Berestetskyy
     */
    public Books findSingleBookForReview(String anIsbn){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Books> q = cb.createQuery(Books.class);
        Root<Books> books = q.from(Books.class);
        ParameterExpression<String> t = cb.parameter(String.class);
        q.select(books).where(cb.equal(books.get("isbn"), t));
        TypedQuery<Books> query = em.createQuery(q).setMaxResults(1);
        query.setParameter(t, anIsbn);
        Books singleBook = query.getSingleResult();  
        return singleBook;
    }
}
