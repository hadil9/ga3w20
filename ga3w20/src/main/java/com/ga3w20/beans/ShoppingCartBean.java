package com.ga3w20.beans;

import com.ga3w20.authentication.UserController;
import com.ga3w20.controllers.BooksJpaController;
import com.ga3w20.controllers.ProvinceJpaController;
import com.ga3w20.controllers.UsersJpaController;
import com.ga3w20.cookies.PreRenderViewBean;
import com.ga3w20.cookies.ShoppingCartCookie;
import com.ga3w20.entities.Books;
import com.ga3w20.entities.Province;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Bean that represents an the shopping cart
 *
 * @author Michael Mishin 1612993
 * @author Franco G. Moro 1738714
 */
@Named
@SessionScoped
public class ShoppingCartBean implements Serializable{
    private final static Logger LOG = LoggerFactory.getLogger(ShoppingCartBean.class); 
    
    private List<String> bookIds;
    
    private BigDecimal sumListPrice;
    private BigDecimal sumSalePrice;
    private BigDecimal sumTax;
    private boolean isEmpty;
    
    @Inject
    private BooksJpaController booksJpaController;
    
    @Inject 
    private ProvinceJpaController provinceJpaController;
    
    @Inject
    private UserController user;
    
    @Inject 
    private UsersJpaController usersJpaController;
    
    @Inject
    private ShoppingCartCookie shoppingCartCookie;
    
    @Inject
    private PreRenderViewBean preRenderView;
    
    /**
     * Default constructor
     */
    public ShoppingCartBean()
    {
        this.bookIds = new ArrayList<>();
    }
    
    /**
     * secondary constructor
     * @param bookidStrings The bookId list
     */
    public ShoppingCartBean(List<String> bookidStrings)
    {
        this.bookIds = bookidStrings;
        isEmpty();
        this.sumListPrice = new BigDecimal(BigInteger.ZERO);
        this.sumSalePrice = new BigDecimal(BigInteger.ZERO);
        this.sumTax = new BigDecimal(BigInteger.ZERO);
    }
    
    /**
     * This method returns the list of book ids
     * 
     * @return The list of book Ids
     */
    public List<String> getBookIds()
    {
        //if cookies are enabled use the shopping cart from cookie
        if(preRenderView.getCookiesEnabled())
        {
            this.bookIds = shoppingCartCookie.checkShoppingList();
        }
        
        return this.bookIds;
    }
    
    /**
     * This method takes the book Ids collected and created a list with the book entities get it from the DB.
     * 
     * @return The Books list.
     */
    public List<Books> getBooks()
    {
        return getBooksFromDB();
    }
    
    public void replaceList(List<String> bookIds)
    {
        this.bookIds = new ArrayList<>(bookIds);
        
    }
    
    /**
     * This method adds a book id to the list
     * 
     * @param bookId The book id to be added to the list
     */
    public void add(String bookId)
    {
        LOG.debug("bookid:"+bookId+" added to the list.");
        bookIds.add(bookId);
        isEmpty();
        // if cookies are enabled add it to the cookie
        if(preRenderView.getCookiesEnabled())
        {
            shoppingCartCookie.writeCookie(bookIds);
        }
    }
    /**
     * This method clears the cart for a user
     * @author Franco G. Moro
     */
    public void clearCart(){
        this.bookIds.clear();
        isEmpty(); 
        shoppingCartCookie.writeCookie(bookIds);
    }
    /**
     * This method adds a book to the cart and takes the user to the Shopping Cart Page.
     * @author Franco G. Moro
     */
    public String buyNow(String bookId){
        add(bookId);
        return "shoppingCart.xhtml?faces-redirect=true";
    }
    /**
     * Removes the bookId from the book Ids list
     * 
     * @param bookId 
     */
    public void remove(String bookId)
    {
        this.bookIds.remove(bookId);
        LOG.debug("The list is now "+this.bookIds.size()+ " and contains:"+ this.bookIds.toString());
        isEmpty();
        // if cookies are enabled update the cookie with the new list
        if(preRenderView.getCookiesEnabled())
        {
            shoppingCartCookie.writeCookie(bookIds);
        }
    }
    
    /**
     * Returns the length of how many items have been saved in the shopping cart
     * 
     * @return The length.
     */
    public int getShoppingListLength()
    {
        return bookIds.size();
    }
    
    /**
     * checks if the shopping list is empty
     * 
     * @return True if it is empty
     */
    public boolean isEmpty()
    {
        if(this.bookIds.size()>0){
            this.isEmpty=false;
        }
        if(this.bookIds.size()<1){
            this.isEmpty=true;
        }
        return this.isEmpty;
    }
    
    /**
     * The Province Name of the logged in user.
     * 
     * @return Province Name
     */
    public String getProvinceName()
    {
        return user.getLoggedUser().getProvinceName();
    }
    
    //============ getting the total list price and sale price sums ============
    
    /**
     * This method will return the total amount of purchase in the shopping cart
     * 
     * @return The sum
     */
    public BigDecimal getShoppingListTotal()
    {
        BigDecimal sum = new BigDecimal(BigInteger.ZERO);
        List<Books> shoppingBookList = this.getBooksFromDB();
        
        LOG.debug("getShoppingListTotal() list size:"+ shoppingBookList.size());
        
        for(int i = 0; i < shoppingBookList.size(); i++)
        {
            sum = sum.add(shoppingBookList.get(i).getListPrice());
            LOG.debug("list price:"+shoppingBookList.get(i).getListPrice());
        }
        
        LOG.debug("total sum is:" + sum.toString());
        return sum;
    }
    
    /**
     * This method will return the total amount of purchase in the shopping cart.
     * The sale amount after reduction.
     * 
     * @return The sum
     */
    public BigDecimal getShoppingSaleTotal()
    {
        BigDecimal sum = new BigDecimal(BigInteger.ZERO);
        List<Books> shoppingBookList = this.getBooksFromDB();
        
        for(int i = 0; i < shoppingBookList.size(); i++)
        {
            sum = sum.add(shoppingBookList.get(i).getSalePrice());
            LOG.debug("list price:"+shoppingBookList.get(i).getSalePrice());
        }
        
        LOG.debug("sale sum is:" + sum.toString());
        return sum;
    }
    
    /**
     * This method will show the customer how much they saved in sales
     * 
     * @return The amount of savings
     */
    public BigDecimal getSavingTotal()
    {
        return getShoppingListTotal().subtract(getShoppingSaleTotal()).negate().setScale(2, RoundingMode.HALF_EVEN);
    }
    
    /**
     * This method will return the grand total for the purchase including tax
     * 
     * @return The Grand Total
     */
    public BigDecimal getTotalPrice()
    {
        BigDecimal sum = getShoppingSaleTotal();
        
        // add the tax
        sum = sum.add(getGST());
        sum = sum.add(getPST());
        sum = sum.add(getQST());
        sum = sum.add(getHST());
        
        return sum;
    }
    
    // TAXES
    public BigDecimal getGST()
    {
        BigDecimal sum = getShoppingSaleTotal();
        
        List<Province> provinces = provinceJpaController.findProvinceByName(user.getLoggedUser().getProvinceName());
        
        return sum.multiply(provinces.get(0).getGst()).setScale(2, RoundingMode.HALF_EVEN);
    }
    
    public BigDecimal getPST()
    {
        BigDecimal sum = getShoppingSaleTotal();
        
        List<Province> provinces = provinceJpaController.findProvinceByName(user.getLoggedUser().getProvinceName());
        
        return sum.multiply(provinces.get(0).getPst()).setScale(2, RoundingMode.HALF_EVEN);
    }
    
    public BigDecimal getQST()
    {
        BigDecimal sum = getShoppingSaleTotal();
        
        List<Province> provinces = provinceJpaController.findProvinceByName(user.getLoggedUser().getProvinceName());
        
        return sum.multiply(provinces.get(0).getQst()).setScale(2, RoundingMode.HALF_EVEN);
    }
    
    public BigDecimal getHST()
    {
        BigDecimal sum = getShoppingSaleTotal();
        
        List<Province> provinces = provinceJpaController.findProvinceByName(user.getLoggedUser().getProvinceName());
        
        return sum.multiply(provinces.get(0).getHst()).setScale(2, RoundingMode.HALF_EVEN);
    }
    
    //============ Helper methods ============
    
    /**
     * This is a helper method to get the books saved in the shopping cart from the db
     * 
     * @return The list of Books
     */
    private List<Books> getBooksFromDB()
    {
        LOG.debug("getting all books for shopping cart");
        
        List<Books> shoppingBookList = new ArrayList<>();
        if(!bookIds.isEmpty())
        {
            bookIds.forEach(bookid ->{
                shoppingBookList.addAll(booksJpaController.findBooks(bookid));
            });
        }
        
        LOG.debug("got "+shoppingBookList.size()+" books.");
        return shoppingBookList;
    }

    public boolean isIsEmpty() {
        return isEmpty;
    }

    public void setIsEmpty(boolean isEmpty) {
        this.isEmpty = isEmpty;
    }
    
     /**
     * check if book is in Shopping cart or not
     * 
     * @param bookIsbn
     * @author Alexander Berestetskyy
     * @return 
     */
    public boolean bookIsInList(String bookIsbn){
        for(String bookInList : bookIds){
            if(bookInList.equals(bookIsbn)){
                return true;
            }
        }
        return false;
    }
}
