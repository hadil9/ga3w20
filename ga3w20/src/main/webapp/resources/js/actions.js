/** 
 * Used to hold the number of stars the client clicked when wrting his review
 * and hold that number, which is between 1 and 5 inclusively
 * 
 * @param idNum
 * @author Alexander Berestetskyy
 */
function colorMyReview(idNum){
  var star = "star";
  for(var i=1;i<=5;i++){
     var id = star.concat(i);
     document.getElementById(id).style.color ="black";
  }
  for(i=5;i>=idNum;i--){
    id = star.concat(i);
    document.getElementById(id).style.color ="#ff820d";
  }
  document.getElementById("ratingNumHidden").value = 5-i;
  displayChar();
}

/**
 * used to color the stars of the averageRating
 * 
 * @param avgRatingDouble
 * @author Alexander Berestetskyy
 */
function colorAvgRating(avgRatingDouble){
    var avgRating = Math.round(avgRatingDouble);
    var avgReview = 'avgReviewstar';
    var starNum = 5;
    for(var y=1;y<= starNum;y++){
        var id = avgReview.concat(y);
        document.getElementById(id).style.color="#ff820d";
        if(y === avgRating){
            break;
        }
    }
}

/**
 * used to color the stars of a review by an user to represent the rating
 * 
 * @param userIndex
 * @param rating
 * @author Alexander Berestetskyy
 */
function colorRatings(userIndex, rating){
    var starNum = 5;
    var reviewstar = 'reviewstar';
    for(var y=1;y <= starNum;y++){
        var id = reviewstar.concat(y,'user',userIndex);
        document.getElementById(id).style.color ="#ff820d";
        if(y === rating){
            break;
        }
    }
}

/**
 * dedicated to "read more" in browsepage
 * 
 * @param clicked_id 
 * @author Alexander Berestetskyy
 */
function expand(clicked_id){
   var arrows = clicked_id;
   var arrowsBox = document.getElementById(clicked_id);
   var str = "description".concat(arrows.substr(3));
   var description = document.getElementById(str);
   if(description.style.height === "" || description.style.height === "0px"){
      description.style.height = "200px";
      arrowsBox.style.marginTop = "300px";
      var reverseArrows = arrowsBox.children;

      for(var i=0;i<reverseArrows.length;i++){
        reverseArrows[i].style.animation = "animatereverse 2s infinite";
      }
   }else{
     description.style.height = "0px";
     arrowsBox.style.marginTop = "100px";
     var reverseArrows = arrowsBox.children;

     for(var i=0;i<reverseArrows.length;i++){
       reverseArrows[i].style.animation = "animateforward 2s infinite";
     }
   }
}

/** 
 * Used to display text when user is writing a review, showing him 
 * if something is missing or not
 * 
 * @author Alexander Berestetskyy
 */
function displayChar(){
        var currval = document.getElementById('userReview').value;
        var validTextarea = document.getElementById('validTextarea');
        var unvalidTextarea = document.getElementById('unvalidTextarea');
        var missingRating = document.getElementById('missingRating');
        var starRating = document.getElementById('ratingNumHidden').value;
        var reviewButton = document.getElementById('reviewbutton');
        if(currval.length === 0){
            validTextarea.style.display="none";
            unvalidTextarea.style.display="inline-block";
        } else if(currval.length < 3000){
            validTextarea.style.display="inline-block";
            unvalidTextarea.style.display="none";
        } else if (validTextarea.length >= 3000){
            validTextarea.style.display="none";
            unvalidTextarea.style.display="inline-block";
        }
        
        if(starRating === ""){
            missingRating.style.display="block";
        }else{
            missingRating.style.display="none";
        }
        
        if(starRating === "" || currval.length === 0 || currval.length >= 3000){
            reviewButton.style.pointerEvents='none';
        } else{
            reviewButton.style.pointerEvents='auto';
        }
   }
