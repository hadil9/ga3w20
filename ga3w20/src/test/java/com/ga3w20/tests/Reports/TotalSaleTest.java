package com.ga3w20.tests.Reports;

import com.ga3w20.beans.Triplet;
import com.ga3w20.controllers.BooksJpaController;
import com.ga3w20.custom.entities.Inventory;
import com.ga3w20.entities.Books;
import java.io.File;
import static org.assertj.core.api.Assertions.assertThat;
import com.ga3w20.exceptions.RollbackFailureException;
import com.ga3w20.tests.JpaController.ParameterRule;
import java.util.List;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.runner.RunWith;
import javax.inject.Inject;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Hadil Elhashani
 */
@RunWith(Arquillian.class)
public class TotalSaleTest {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(TotalSaleTest.class);

    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The webArchive is the special packaging of your project
        // so that only the test cases run on the server or embedded
        // container
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(Books.class.getPackage())
                .addPackage(BooksJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/test/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createTables.sql")
                .addClass(ParameterRule.class)
                .addClass(Inventory.class)
                .addClass(Triplet.class)
                .addAsLibraries(dependencies);

        return webArchive;
    }

    @Inject
    private BooksJpaController booksController;

    private Triplet<String, String, Integer> dateRange;

    @Rule
    public ParameterRule rule = new ParameterRule("dateRange",
            new Triplet<String, String, Integer>("2020-01-01", "2020-01-01", 13),
            new Triplet<String, String, Integer>("2020-01-01", "2020-01-07", 60),
            new Triplet<String, String, Integer>("2020-01-04", "2020-01-12", 69),
            new Triplet<String, String, Integer>("2020-02-01", "2020-02-11", 75),
            new Triplet<String, String, Integer>("2020-01-17", "2020-01-24", 49),
            new Triplet<String, String, Integer>("2020-01-01", "2020-02-01", 98));

    @Test
    public void findRowCount() {
        List totalSales = this.booksController.findTotalSales(dateRange.getValue0(), dateRange.getValue1());
        assertThat(totalSales).hasSize(dateRange.getValue2());
    }

}