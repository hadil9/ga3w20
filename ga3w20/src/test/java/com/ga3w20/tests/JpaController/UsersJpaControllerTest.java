package com.ga3w20.tests.JpaController;

import com.ga3w20.controllers.UsersJpaController;
import com.ga3w20.entities.Users;
import com.ga3w20.exceptions.RollbackFailureException;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import javax.inject.Inject;
import java.io.*;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Ignore;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Yongchao
 */
@RunWith(Arquillian.class)
public class UsersJpaControllerTest {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(UsersJpaControllerTest.class);

    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The webArchive is the special packaging of your project
        // so that only the test cases run on the server or embedded
        // container
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(Users.class.getPackage())
                .addPackage(UsersJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/test/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createTables.sql")
                .addAsLibraries(dependencies);

        return webArchive;
    }

    @Inject
    private UsersJpaController usersJpa;

    @Test
    public void FindAllEntities() {        
        List<Users> entityList = usersJpa.findUsersEntities();
        LOG.info("Total users number: ", entityList.size());
        assertThat(entityList).hasSize(100);
    }
    
    @Test
    public void FindUserById() {        
        List<Users> entityList = usersJpa.findUsersById("1");
        LOG.info("Total users number: ", entityList.size());
        assertThat(entityList).hasSize(1);
    }
    
    @Test
    public void findUsersByCellphone() {        
        List<Users> entityList = usersJpa.findUsersByCellphone("59");
        LOG.info("Total users number: ", entityList.size());
        assertThat(entityList).hasSize(11);
    }
    
    @Test
    public void findUsersByCity() {        
        List<Users> entityList = usersJpa.findUsersByCity("mon");
        LOG.info("Total users number: ", entityList.size());
        assertThat(entityList).hasSize(11);
    }


    @Test
    public void findUsersByCompany() {        
        List<Users> entityList = usersJpa.findUsersByCompany("al");
        LOG.info("Total users number: ", entityList.size());
        assertThat(entityList).hasSize(3);
    }
    
    @Test
    public void findUsersByEmail() {        
        List<Users> entityList = usersJpa.findUsersByEmail("google");
        LOG.info("Total users number: ", entityList.size());
        assertThat(entityList).hasSize(3);
    }
    
    @Test
    public void findUsersByFirstName() {        
        List<Users> entityList = usersJpa.findUsersByFirstName("al");
        LOG.info("Total users number: ", entityList.size());
        assertThat(entityList).hasSize(8);
    }
    
    @Test
    public void findUsersByHomeTelephone() {        
        List<Users> entityList = usersJpa.findUsersByHomeTelephone("514");
        LOG.info("Total users number: ", entityList.size());
        assertThat(entityList).hasSize(2);
    }
    
    @Test
    public void findUsersByLastName() {        
        List<Users> entityList = usersJpa.findUsersByLastName("al");
        LOG.info("Total users number: ", entityList.size());
        assertThat(entityList).hasSize(3);
    }
    
    @Test
    public void findUsersByPostalCode() {        
        List<Users> entityList = usersJpa.findUsersByPostalCode("h2");
        LOG.info("Total users number: ", entityList.size());
        assertThat(entityList).hasSize(1);
    }
    
    @Test
    public void findUsersByProvince() {        
        List<Users> entityList = usersJpa.findUsersByProvince("que");
        LOG.info("Total users number: ", entityList.size());
        assertThat(entityList).hasSize(7);
    }
    
    
    /**
     * Based on method getUsersCount in UsersJpaController, which retrieves the number of users stored
     * in the database.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void GetUsersCount(){
        int numOfUsers = usersJpa.getUsersCount();
        assertThat(numOfUsers).isEqualTo(100);
    }
    
     /**
     * Based on method findIdOfUser in UsersJpaController, which retrieves an user, used for his id, based on
     * the email passed as an argument.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindIdOfUser(){
        Users user1 = usersJpa.findIdOfUser("cst.send@gmail.com");
        assertThat(user1.getId()).isEqualTo(1);
        Users user2 = usersJpa.findIdOfUser("cst.receive@gmail.com");
        assertThat(user2.getId()).isEqualTo(2);
    }

}
